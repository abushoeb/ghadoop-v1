#!/usr/bin/python

import threading
import time

from datetime import datetime,timedelta
from optparse import OptionParser
from itertools import *

from ghadoopcommons import *
from ghadooplogger import *
from ghadoopwaiting import *
from ghadoopreplication import *
from ghadoopmonitor import *
from ghdfsmonitor import *
from ghadoop import *

import sys
#sys.path.append(r'greenavailability/')
#import model,setenv
# Abu Shoeb modified
#sys.path.append(r'/home/goiri/greenDC/gslurm/greenavailability/')
sys.path.append(r'/home/shoeb/greenDC/gslurm/greenavailability/')
import model_cloudy,setenv


setenv.init()



# Extra functions
# Get the available green power
def readGreenAvailFile(filename):
	greenAvailability = []
	file = open(filename, 'r')
	for line in file:
		if line != '' and line.find("#")!=0 and line != '\n':
			lineSplit = line.strip().expandtabs(1).split(' ')
			t=lineSplit[0]
			p=float(lineSplit[1])
			# Apply scale factor TODO
			p = (p/2300.0)*MAX_POWER
			
			greenAvailability.append(TimeValue(t,p))
	file.close()
	return greenAvailability

# Get the cost of the brown energy
def readBrownPriceFile(filename):
	brownPrice = []
	file = open(filename, 'r')
	for line in file:
		if line != '' and line != '\n' and line.find("#")!=0:
			lineSplit = line.strip().expandtabs(1).split(' ')
			t=lineSplit[0]
			p=float(lineSplit[1])
			brownPrice.append(TimeValue(t,p))
	file.close()
	return brownPrice

# Read workload file
def readWorkload(filename):
	workload = []
	if filename != None:
		file = open(filename, 'r')
		for line in file:
			if line != '' and line != '\n' and line.find("#")!=0:
				line = line.strip().expandtabs(1)
				while line.find("  ")>=0:
					line = line.replace("  ", " ");
				lineSplit = line.strip().expandtabs(1).split(' ')
				# Treat date
				lineSplit[0] = parseTime(lineSplit[0])
				workload.append(lineSplit)
	return sorted(workload)

def submitWorkload(time, workload, waiting=True):
	#done = False
	#firstJobId = None
	threads = []
	while len(workload)>0 and parseTime(workload[0][0])<=time:
		priority = workload[0][1]
		input = workload[0][2].split(",")
		output = workload[0][3].split(",")
		cmd = workload[0][4]
		for i in range(5,len(workload[0])):
			cmd+=" "+workload[0][i]
		
		thread = threading.Thread(target=submitJob,args=(None, cmd, input, output, priority, waiting))
		thread.start()
		
		workload.pop(0)
		
	# Wait for everything to be submitted
	#while len(threads)>0:
		#threads.pop().join()

def numScheduledJobs(sched):
	ret = 0
	if sched != None:
		for i in range(0, len(sched)):
			ret += len(sched[i])
	return ret
	
	
def numJobs():
	ret = 0
	jobs = getJobsFast()
	if jobs != None:
		for job in jobs.values():
			ret += 1
			if job.state=="COMPLETED" or job.state.startswith("CANCEL"):
				ret -= 1
	return ret

def timeLimit(timeElapsed):
	ret = True
	if TIME_LIMIT != None:
		if timeElapsed < TIME_LIMIT:
			ret = False
	return ret

# Thread that submits jobs
class JobSubmitter(threading.Thread):
	def __init__(self, timeStart, waiting=True):
		threading.Thread.__init__(self)
		self.running = True
		self.timeStart = timeStart
		self.waiting = waiting

	def kill(self):
		self.running = False
	
	def run(self):
		while self.running:
			submitWorkload(self.getCurrentTime(), workload, self.waiting)
			time.sleep(0.2)

	def getCurrentTime(self):
		timeNow = datetime.now()
		timeNow = datetime(timeNow.year, timeNow.month, timeNow.day, timeNow.hour, timeNow.minute, timeNow.second)
		return toSeconds(timeNow-self.timeStart)

class GreenHadoopData():
	def __init__(self):
		self.greenAvailArray = []
		self.greenPrediArray = []
		self.brownPriceArray = []
		self.reqPower = 0.0
		
	def getBrownPrice(self):
		return self.brownPriceArray[0]

	def getGreenAvail(self):
		return self.greenAvailArray[0]
	
	def getGreenPredi(self):
		return self.greenPrediArray[4]

	def getReqPower(self):
		return self.reqPower


# Start daemon...
# Parse parameters
parser = OptionParser()
parser.add_option("-w", "--workload", dest="workloadFile", help="specify workload file", type="string", default=WORKLOAD_FILE)
parser.add_option("-b", "--brownfile", dest="brownFile", help="specify brown price file", type="string", default=BROWN_PRICE_FILE)
parser.add_option("-g", "--greenfile", dest="greenFile", help="specify green availability file", type="string", default=None)
parser.add_option("-d", "--date", dest="date", help="specify date", type="string", default=None)
parser.add_option("-x", "--speedup", dest="speedup", help="specify speedup", type="float", default=1.0)
#parser.add_option("--pred", dest="predGreen", help="specify if the scheduler use green availability predictions", type="int", default=None)
parser.add_option("--pred", action="store_true", dest="predGreen", help="specify if the scheduler use green availability predictions")
parser.add_option("--energy", action="store_true", dest="schedEnergy", help="specify if the scheduler supports energy scheduling")
parser.add_option("--nogreen", action="store_true", dest="schedNoGreen", help="specify if the scheduler supports green energy")
parser.add_option("--nobrown", action="store_true", dest="schedNoBrown", help="specify if the scheduler supports brown pricing")
parser.add_option("--peak", dest="peakCost",  type="float", help="specify if the scheduler supports brown pricing", default=None)
parser.add_option("--repl", dest="replThreads",  type="int", help="specify the number of replications threads", default=40)
parser.add_option("--taskenergy", dest="taskEnergy",  type="float", help="specify the energy per task", default=0.44)


(options, args) = parser.parse_args()
workloadFile = options.workloadFile
brownPriceFile = options.brownFile
greenAvailFile = options.greenFile
speedup = options.speedup

if options.date == None or options.date == "-" or options.date == "test":
	baseDate = BASE_DATE
else:
	baseDate = datetime.strptime(options.date, "%Y-%m-%dT%H:%M:%S")

# Show starting info
writeLog("logs/ghadoop-energy.log", "# Options = "+str(options))
print "Date:        "+str(baseDate)
if options.date == "-":
	print "No energy"
elif options.date == "test":
	print "Testing"
print "Max time:    "+toTimeString(TOTALTIME)
print "Slot length: "+toTimeString(SLOTLENGTH)
print "Slots:       "+str(numSlots)
print "Workload:    "+str(workloadFile)
print "Brown price: "+str(brownPriceFile)
print "Green avail: "+str(greenAvailFile)
print "Speedup:     "+str(speedup)+"x"


# Predictor
if options.predGreen == True:
	#ep = model.EnergyPredictor('./greenavailability', 2, MAX_POWER) # Threshold = 2hour; Capacity=2000W
	#ep = model.CachedEnergyPredictor(baseDate, predictionHorizon=48,path='./greenavailability', threshold=2, scalingFactor=MAX_POWER) # Threshold = 2hour; Capacity=2037W
	#ep = model.CachedEnergyPredictor(baseDate, predictionHorizon=48, path='./greenavailability', threshold=2, scalingFactor=MAX_POWER, useActualData=True, error_exit=model.enter_on_thresh)
	#ep = model.CachedEnergyPredictor(baseDate, predictionHorizon=48, path='./greenavailability', threshold=2, scalingFactor=MAX_POWER, useActualData=True, error_exit=options.predGreen)
	# Abu Shoeb modified	
	#ep = model_cloudy.CachedEnergyPredictor(baseDate, predictionHorizon=(TOTALTIME*speedup/3600), path='/home/goiri/greenDC/gslurm/greenavailability/', threshold=2, scalingFactor=MAX_POWER, useActualData=True, error_exit=model_cloudy.normal)
	ep = model_cloudy.CachedEnergyPredictor(baseDate, predictionHorizon=(TOTALTIME*speedup/3600), path='/home/shoeb/greenDC/gslurm/greenavailability/', threshold=2, scalingFactor=MAX_POWER, useActualData=True, error_exit=model_cloudy.normal)

	#model.enter_on_thresh
	print "Getting predictions... done!"

# cleaing output and temporary data
# Abu Shoeb modified
# rmFile("/user/goiri/output*")
# rmFile("/user/goiri/crawlMain/new_indices")
rmFile("/user/shoeb/output*")
rmFile("/user/shoeb/crawlMain/new_indices")


# Start monitoring the Hadoop system
monitorMapred = MonitorMapred()
monitorMapred.start()
monitorHdfs = MonitorHDFS()
monitorHdfs.start()

# Read workload
workload = readWorkload(workloadFile)

# Initialize matrices
data = GreenHadoopData()
#greenAvailArray = []
#greenPrediArray = []
#brownPriceArray = []
for i in range(0, numSlots):
	data.greenAvailArray.append(0.0)
	data.greenPrediArray.append(0.0)
	data.brownPriceArray.append(0.0)

# Read green availability and brown cost
if options.date == "-":
	print "Reading green availability from: No green energy"
	greenAvail = readGreenAvailFile("data/greenpower.none")
#elif options.date == "test":
elif greenAvailFile != None:
	print "Reading green availability from: "+str(greenAvailFile)
	greenAvail = readGreenAvailFile(greenAvailFile)
	#greenAvail = readGreenAvailFile("data/greenpower.solar")
else:
	print "Reading green availability from: data/solarpower-%02d-%02d-%02d" % (baseDate.day, baseDate.month, baseDate.year)
	greenAvail = readGreenAvailFile("data/solarpower-%02d-%02d-%02d" % (baseDate.day, baseDate.month, baseDate.year))
print "Reading brown price from "+str(brownPriceFile)
brownPrice = readBrownPriceFile(brownPriceFile)

# Starting...
timeStart = datetime.now()
timeStart = datetime(timeStart.year, timeStart.month, timeStart.day, timeStart.hour, timeStart.minute, timeStart.second)


sched = None
notScheduled = []
peakBrown = 0.0 # W
peakBrown = POWER_IDLE_GHADOOP + 16*Node.POWER_IDLE # W
peakBrownAccount = []

# Start loggers
schedulerLogger = SchedulerLogger(timeStart)
schedulerLogger.start()

jobLogger = JobLogger(timeStart)
jobLogger.start()

energyLogger =  EnergyLogger(timeStart, data)
energyLogger.start()

# Start daemons
useWaitingQueue = True
if options.schedEnergy != True:
	useWaitingQueue = False
jobsubmitter = JobSubmitter(timeStart, useWaitingQueue)
jobsubmitter.start()

waitingqueuemanager = WaitingQueueManager(timeStart)
waitingqueuemanager.start()

# Start replication thread
replicationthread = ReplicationThread(timeStart, options.replThreads)
replicationthread.start()

cleandatathread = CleanDataThread(timeStart)
cleandatathread.start()

# To kill the threads
signal.signal(signal.SIGINT, signal_handler)



# Do a cycle while there is something to execute
timeElapsed = 0
lastPercentageLog = 0
#lastPercentageLog = datetime.now()
#while len(workload)>0 or numJobs()>0 or not timeLimit(timeElapsed):
#while len(workload)>0 or numJobs()>0 or not timeLimit(timeElapsed):
while not timeLimit(timeElapsed):
	# Compute where we are
	timeNow = datetime.now()
	timeNow = datetime(timeNow.year, timeNow.month, timeNow.day, timeNow.hour, timeNow.minute, timeNow.second)
	timeElapsed = toSeconds(timeNow-timeStart)
	timeElapsedSpeedup = int(timeElapsed*speedup)
	dateCurrent = baseDate + timedelta(seconds=timeElapsedSpeedup)
	slotCurrent = int(math.floor(1.0*timeElapsed/SLOTLENGTH))
	
	# Report percentage every 15 minutes
	if (timeElapsed-lastPercentageLog) > 15*60:
		print "%.1f%%" % (100.0*timeElapsed/TIME_LIMIT)
		lastPercentageLog = timeElapsed

	if DEBUG>0 and SCHEDULE_SLOT:
		clearscreen()
		band=""
		for i in range(0, 80):
			band+="="
		print bcolors.WHITEBG+"==="+bcolors.ENDC+"Date:"+str(dateCurrent)+" Time:"+toTimeString(timeElapsedSpeedup)+"("+toTimeString(timeElapsed)+") "+str(speedup)+"x Workload:"+str(len(workload))+" Waiting:"+str(len(waitingQueue))+" "+bcolors.WHITEBG+band+bcolors.ENDC
	elif DEBUG>0:
		print "Date: "+str(dateCurrent)+"  Time: "+toTimeString(timeElapsedSpeedup)
	
	# Compute inputs: green availability and brown costW
	# Get data from forecasts
	if options.predGreen != None:
		#date = baseDate + timedelta(seconds=timeElapsedSpeedup)
		#prediction, flag = ep.getGreenAvailability(date, int(TOTALTIME*speedup/3600)) # 48h
		prediction, flag = ep.getGreenAvailability(dateCurrent, int(TOTALTIME*speedup/3600)) # 48h
		
		# Manage data: put it in the green availability matrix
		greenPredi = []
		for i in range(0, len(prediction)):
			d = dateCurrent + timedelta(hours=i)
			if i>0:
				d = datetime(d.year, d.month, d.day, d.hour)
			#TODO
			#t = toSeconds(d-dateCurrent) + timeElapsed
			t = toSeconds(d-dateCurrent) + timeElapsedSpeedup
			value = prediction[i]
			greenPredi.append(TimeValue(t, value))
	else:
		greenPredi = greenAvail

	# Process input information: green and brown energy
	jG1 = jP1 = jB1 = 0
	jG2 = jP2 = jB2 = 0
	for i in range(0, numSlots):
		start = i*SLOTLENGTH + timeElapsed
		start *= speedup
		end = (i+1)*SLOTLENGTH + timeElapsed
		end *= speedup

		# Green prediction: estimate average energy available
		while jP1+1 < len(greenPredi) and (greenPredi[jP1+1].t) <= start:
			jP1+=1
		while jP2+1 < len(greenPredi) and (greenPredi[jP2+1].t) <= end:
			jP2+=1
		if start>=greenPredi[jP2].t:
			data.greenPrediArray[i] = greenPredi[jP2].v
		else:
			greenV = 0.0
			for j in range(jP1, jP2+1):
				j1 = greenPredi[j].t
				j2 = greenPredi[j].t
				if j+1 < len(greenPredi):
					j2 = greenPredi[j+1].t
				if j2 > end:
					j2 = end
				if j1 > end:
					j1 = end
				if j1 < start:
					j1 = start
				greenV += (greenPredi[j].v)*(j2-j1)
			# Multiply by speedup as the slot is different than the time itself
			data.greenPrediArray[i] = greenV/(SLOTLENGTH*speedup)
		
		# Green availability: estimate average energy available
		while jG1+1 < len(greenAvail) and (greenAvail[jG1+1].t) <= start:
			jG1+=1
		while jG2+1 < len(greenAvail) and (greenAvail[jG2+1].t) <= end:
			jG2+=1
		if start>=greenAvail[jG2].t:
			data.greenAvailArray[i] = greenAvail[jG2].v
		else:
			greenV = 0.0
			for j in range(jG1, jG2+1):
				j1 = greenAvail[j].t
				j2 = greenAvail[j].t
				if j+1 < len(greenAvail):
					j2 = greenAvail[j+1].t
				if j2 > end:
					j2 = end
				if j1 > end:
					j1 = end
				if j1 < start:
					j1 = start
				greenV += (greenAvail[j].v)*(j2-j1)
			data.greenAvailArray[i] = greenV/(SLOTLENGTH*speedup)
		
		# Use actual value
		#if options.schedNoGreen != False:
		data.greenPrediArray[0] = data.greenAvailArray[0]
		
		# Brown: current price
		while jB1+1 < len(brownPrice) and (brownPrice[jB1+1].t) <= start:
			jB1+=1
		data.brownPriceArray[i] = brownPrice[jB1].v

	if DEBUG >=4:
		print "Green energy available:"
		max = len(getNodes()) * Node.POWER_FULL
		for i in range(MAXSIZE,0,-1):
			out=""
			# TODO change the scale factor of the figure
			#for v in data.greenAvailArray:
			scale = 2
			for j in range(0, len(data.greenAvailArray)/scale):
				index = j*scale
				v = data.greenAvailArray[index]
				if v > (1.0*(i-1)*max/MAXSIZE):
					out += bcolors.GREENBG+" "+bcolors.ENDC
				else:
					out += " "
			print out+" %.2fW" % (1.0*i*max/MAXSIZE)
			
		print "Green energy predicted: "+str(dateCurrent)
		max = len(getNodes()) * Node.POWER_FULL
		for i in range(MAXSIZE,0,-1):
			out=""
			# TODO change the scale factor of the figure
			#for v in data.greenPrediArray:
			scale = 2
			for j in range(0, len(data.greenPrediArray)/scale):
				index = j*scale
				v = data.greenPrediArray[index]
				if v > (1.0*(i-1)*max/MAXSIZE):
					out += bcolors.GREENBG+" "+bcolors.ENDC
				else:
					out += " "
			print out+" %.2fW" % (1.0*i*max/MAXSIZE)

	# Schedule jobs
	if SCHEDULE_SLOT or len(notScheduled)>0:
		# Perform scheduling
		if getPhase() == PHASE_CLEAN:
			data.reqPower = 0.0
		elif options.schedEnergy == True:
			#timeSchedule = datetime.now()
			data.reqPower = schedule(timeElapsed, peakBrown, data.greenPrediArray, data.brownPriceArray, options)
			#timeSchedule = datetime.now() - timeSchedule
			#print str(timeElapsed)+" schedule: "+str(timeSchedule)
		else:
			data.reqPower = len(getNodes())*Node.POWER_FULL + POWER_IDLE_GHADOOP
		# Dispatch actions
		done = dispatch(data.reqPower, data.greenPrediArray[0])
		print "dispatch job: "+str(done)

	# Peak power TODO
	# Calculate used energy and power
	#greenEnergyAvail = data.greenAvailArray[0] # Wh
	#brownReqPower = 0.0
	#if data.reqPower>0.0:
		#greenPowerAvail = greenEnergyAvail
		#if data.reqPower>greenEnergyAvail:
			#brownReqPower = data.reqPower-greenEnergyAvail
	# Update peak power
	brownReqPower = energyLogger.getReqBrownPower()
	peakBrownAccount.append(TimeValue(timeElapsed, brownReqPower))
	nextMeasure = timeElapsed+SLOTLENGTH
	timeMeasure = 0.0
	totaMeasure = 0.0
	for tv in reversed(peakBrownAccount):
		#if timeMeasure+(nextMeasure-tv.t) > PEAK_PERIOD/speedup:
			#break
		if timeMeasure > PEAK_PERIOD/speedup:
			break
		else:
			totaMeasure += (nextMeasure-tv.t)*tv.v
			timeMeasure += (nextMeasure-tv.t)
			nextMeasure = tv.t
	while len(peakBrownAccount)>1 and (peakBrownAccount[len(peakBrownAccount)-1].t-peakBrownAccount[1].t)>PEAK_PERIOD/speedup:
		peakBrownAccount.pop(0)
	if timeMeasure < PEAK_PERIOD/speedup:
		timeMeasure = PEAK_PERIOD/speedup
		
	brownReqPower = totaMeasure/timeMeasure
	
	# Is it a new peak?
	#maxPower = len(getNodes())*Node.POWER_FULL + POWER_IDLE_GHADOOP
	if 0.95*brownReqPower > peakBrown:
		peakBrown = 0.95*brownReqPower
	# We are actually requiring the maximum....
	#if brownReqPower >= 0.95*maxPower:
		#peakBrown = maxPower
	
	if DEBUG>0:
		print "Power: brown=%.2fW peak=%.2fW" % (brownReqPower, peakBrown)
	
	# Wait for next cycle: next slot or event
	wait = True
	while wait:
		# Wait 200 ms
		time.sleep(0.2)
		
		# Update every slot
		if CYCLE_SLOT:
			# Update time
			auxNow = datetime.now()
			auxNow = datetime(auxNow.year, auxNow.month, auxNow.day, auxNow.hour, auxNow.minute, auxNow.second)
			timeElapsed = toSeconds(auxNow-timeStart)
			# Next slot?
			if timeElapsed >= (slotCurrent+1)*SLOTLENGTH:
				wait = False


# Compute final time
timeNow = datetime.now()
timeNow = datetime(timeNow.year, timeNow.month, timeNow.day, timeNow.hour, timeNow.minute, timeNow.second)
timeElapsed = toSeconds(timeNow-timeStart)
print "Total experiment time: "+toTimeString(timeElapsed)

# Finishing threads
killingThreads = {}
schedulerLogger.kill()
killingThreads["scheduler"] = (schedulerLogger, datetime.now())
jobLogger.kill()
killingThreads["job"] = (jobLogger, datetime.now())
energyLogger.kill()
killingThreads["energy"] = (energyLogger, datetime.now())

jobsubmitter.kill()
killingThreads["jobsubmitter"] = (energyLogger, datetime.now())
waitingqueuemanager.kill()
killingThreads["waitingqueue"] = (energyLogger, datetime.now())
replicationthread.kill()
killingThreads["replication"] = (energyLogger, datetime.now())
cleandatathread.kill()
killingThreads["cleandata"] = (energyLogger, datetime.now())

monitorMapred.kill()
killingThreads["monitorMapred"] = (energyLogger, datetime.now())
monitorHdfs.kill()
killingThreads["monitorHdfs"] = (energyLogger, datetime.now())

# Waiting for threads to end
print "Waiting for threads to end..."
while len(killingThreads)>0:
	for threadType in list(killingThreads.keys()):
		thread,timeout = killingThreads[threadType]
		thread.join(1.0)
		if not thread.isAlive():
			if threadType=="scheduler":
				print "\tScheduler logger... OK"
			elif threadType=="job":
				print "\tJob logger... OK"
			elif threadType=="energy":
				print "\tEnergy logger... OK"
			elif threadType=="jobsubmitter":
				print "\tJob submitter... OK"
			elif threadType=="waitingqueue":
				print "\tWaiting queue... OK"
			elif threadType=="replication":
				print "\tReplication... OK"
			elif threadType=="cleandata":
				print "\tClean data... OK"
			elif threadType=="monitorMapred":
				print "\tMonitoring MapReduce... OK"
			elif threadType=="monitorHdfs":
				print "\tMonitoring HDFS... OK"
			del killingThreads[threadType]
		elif (datetime.now() - timeout)>timedelta(seconds=10):
			print "Error! "+str(threadType)+" has not terminated.... kill him!"

# Close loggers
closeLogs()

sys.exit(0)
