set term svg size 960,450
set out "logs/11-900secs-greenpeak-day1-30seconds/11-900secs-greenpeak-day1-30seconds-nodes.svg"
set ylabel "Nodes"
set yrange [0:16]
set xdata time
set timefmt "%s"
set format x "%a\n%R"
set format x "%a %R"
set style fill solid
plot "logs/11-900secs-greenpeak-day1-30seconds/11-900secs-greenpeak-day1-30seconds-energy.log" using ($1*24):7 lw 2 lc rgb "#C0C0C0" w filledcurve title "Dec nodes",\
"logs/11-900secs-greenpeak-day1-30seconds/11-900secs-greenpeak-day1-30seconds-energy.log" using ($1*24):6 lc rgb "#909090" w filledcurve title "Up nodes",\
"logs/11-900secs-greenpeak-day1-30seconds/11-900secs-greenpeak-day1-30seconds-energy.log" using ($1*24):5 lc rgb "#404040" w filledcurve title "Run nodes"
