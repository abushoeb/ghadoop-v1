#!/bin/bash

# Abu Shoeb modified
export HADOOP_HOME="/home/shoeb/hadoop-0.21.0"
# export HADOOP_HOME="/home/goiri/hadoop-0.21.0"
# export HADOOP_HOME="/home/as2352/hadoop-0.21.0"

function experiment {
	NAME=$1
	shift
	FLAGS=$*

	echo "name="$NAME" flags="$FLAGS

	# Abu Shoeb modified
	touch $HADOOP_HOME/logs/hadoop-shoeb-jobtracker-shoeb-dell.log
	touch $HADOOP_HOME/logs/hadoop-shoeb-namenode-shoeb-dell.log
	#touch $HADOOP_HOME/logs/hadoop-shoeb-jobtracker-shoeb-dell.rutgers.edu.log
	#touch $HADOOP_HOME/logs/hadoop-shoeb-namenode-shoeb-dell.rutgers.edu.log
	# touch $HADOOP_HOME/logs/hadoop-goiri-jobtracker-crypt01.log
	# touch $HADOOP_HOME/logs/hadoop-goiri-namenode-crypt01.log
	
	# Clean system
	echo "Cleanning..."
	cleaned=0
	retries=3
	while (( "$cleaned" == "0" )) && (( "$retries" > "0" )); do
		cleaned=1
#Abu Shoeb modified
#		./ghadoopclean.py > /dev/null &
 		./ghadoopclean.py &
		# > /dev/null &
		TESTPID=$!
		a=0
		while ps -p $TESTPID >/dev/null; do
			sleep 1
			let a=a+1
			if [ $a -gt 400 ]; then
				echo "Clean failed: try again..."
				kill -9 $TESTPID
				cleaned=0
				let retries=retries-1
			fi
		done
	done
	sleep 1
	echo "Starting..."

	rm -f logs/ghadoop-jobs.log
	rm -f logs/ghadoop-energy.log
	rm -f logs/ghadoop-scheduler.log
	rm -f logs/ghadoop-error.log
	touch logs/ghadoop-error.log
	sleep 1
	./ghadoopd $FLAGS

	# Save results
	mkdir -p logs/$NAME
# 	echo $NAME > logs/$NAME/$NAME-summary.log
	./ghadoopparser.py logs/ghadoop >> logs/$NAME/$NAME-summary.html

	mv logs/ghadoop-jobs.log logs/$NAME/$NAME-jobs.log
	mv logs/ghadoop-energy.log logs/$NAME/$NAME-energy.log
	mv logs/ghadoop-scheduler.log logs/$NAME/$NAME-scheduler.log
	mv logs/ghadoop-error.log logs/$NAME/$NAME-error.log

	# Plot results: energy
# 	echo "set term svg size 1280,600" > logs/$NAME/$NAME.plot
	echo "set term svg size 960,450" > logs/$NAME/$NAME.plot
	echo "set out \"logs/$NAME/$NAME-energy.svg\"" >> logs/$NAME/$NAME.plot
	echo "set ylabel \"Power (kW)\"" >> logs/$NAME/$NAME.plot
# 	echo "set yrange [0:1.8]" >> logs/$NAME/$NAME.plot
	echo "set yrange [0:2.4]" >> logs/$NAME/$NAME.plot

	echo "set y2label \"Brown energy price ($/kWh)\"" >> logs/$NAME/$NAME.plot
	echo "set y2range [0:0.3]" >> logs/$NAME/$NAME.plot
	echo "set y2tics" >> logs/$NAME/$NAME.plot
	echo "set ytics nomirror" >> logs/$NAME/$NAME.plot

	echo "set xdata time" >> logs/$NAME/$NAME.plot
	echo "set timefmt \"%s\"" >> logs/$NAME/$NAME.plot
	echo "set format x \"%a\n%R\"" >> logs/$NAME/$NAME.plot
	echo "set format x \"%a %R\"" >> logs/$NAME/$NAME.plot

	echo "set style fill solid" >> logs/$NAME/$NAME.plot
	echo "plot \"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$10/1000) lc rgb \"#808080\" w filledcurve title \"Brown consumed\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$8/1000) lc rgb \"#e6e6e6\" w filledcurve title \"Green consumed\",\\" >> logs/$NAME/$NAME.plot
# 	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1+(2*24*24+10*24)):(\$2/1000) lw 2 lc rgb \"black\" w steps title \"Green predicted\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$3/1000) lw 2 lc rgb \"black\" w steps title \"Green predicted\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):4 axes x1y2 lw 2 lc rgb \"black\" w steps title \"Brown price\"" >> logs/$NAME/$NAME.plot

	gnuplot logs/$NAME/$NAME.plot

	# Plot results: nodes
# 	echo "set term svg size 1280,600" > logs/$NAME/$NAME.plot
	echo "set term svg size 960,450" > logs/$NAME/$NAME-nodes.plot
	echo "set out \"logs/$NAME/$NAME-nodes.svg\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set ylabel \"Nodes\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set yrange [0:16]" >> logs/$NAME/$NAME-nodes.plot

	echo "set xdata time" >> logs/$NAME/$NAME-nodes.plot
	echo "set timefmt \"%s\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set format x \"%a\n%R\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set format x \"%a %R\"" >> logs/$NAME/$NAME-nodes.plot

	echo "set style fill solid" >> logs/$NAME/$NAME-nodes.plot
	echo "plot \"logs/$NAME/$NAME-energy.log\" using (\$1*24):7 lw 2 lc rgb \"#C0C0C0\" w filledcurve title \"Dec nodes\",\\" >> logs/$NAME/$NAME-nodes.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):6 lc rgb \"#909090\" w filledcurve title \"Up nodes\",\\" >> logs/$NAME/$NAME-nodes.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):5 lc rgb \"#404040\" w filledcurve title \"Run nodes\"" >> logs/$NAME/$NAME-nodes.plot

	gnuplot logs/$NAME/$NAME-nodes.plot
}



# DATE_MOST="2010-5-31T09:00:00"
# SOLAR_MOST="data/solarpower-31-05-2010" # Best energy
BROWN_SUMMER="data/browncost-onoffpeak-summer.nj"
BROWN_WINTER="data/browncost-onoffpeak-winter.nj"

# High High
DATE_1="2011-5-9T00:00:00"
SOLAR_1="data/solarpower-09-05-2011"
BROWN_1=$BROWN_WINTER

# High Medium
DATE_2="2011-5-12T00:00:00"
SOLAR_2="data/solarpower-12-05-2011"
BROWN_2=$BROWN_WINTER

# Medium High
DATE_3="2011-6-14T00:00:00"
SOLAR_3="data/solarpower-14-06-2011"
BROWN_3=$BROWN_SUMMER

# Medium Medium
DATE_4="2011-6-16T00:00:00"
SOLAR_4="data/solarpower-16-06-2011"
BROWN_4=$BROWN_SUMMER

# Low Low
DATE_0="2011-5-15T00:00:00"
SOLAR_0="data/solarpower-15-05-2011"
BROWN_0=$BROWN_WINTER


PEAK_WINTER=5.5884 # Winter October-May
PEAK_SUMMER=13.6136 # Summer June-Sep

# Workloads
# WORKLOAD_CONT_015="workload/workload-continuous-015"
WORKLOAD_CONT_030="workload/workload-continuous-030-shoeb"
WORKLOAD_CONT_SHOEB="workload/workload-continuous-shoeb"
# WORKLOAD_CONT_045="workload/workload-continuous-045"
WORKLOAD_CONT_060="workload/workload-continuous-060"
# WORKLOAD_CONT_075="workload/workload-continuous-075"
# WORKLOAD_CONT_090="workload/workload-continuous-090"
WORKLOAD_CONT_100="workload/workload-continuous-100"

WORKLOAD_NUTCH="workload/workload-nutch-030"
WORKLOAD_NUTCH2="workload/workload-nutch-008"

WORKLOAD_HIGH1="workload/workload-continuous-high1-3"
WORKLOAD_HIGH2="workload/workload-continuous-high2-3"

TASK_ENERGY_015=0.30
TASK_ENERGY_030=0.35
TASK_ENERGY_060=0.45
TASK_ENERGY_090=0.55

TASK_ENERGY_NUTCH=0.14

REPLICATION=30

# After submission
# TODO redo 22 to 23 for brown price
# TODO redo number of reduces


# Experiments start

# experiment "20-hadoop-day1-30seconds"             --nobrown --nogreen --peak 0.0 --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_030
# experiment "21-hadooppm-day1-30seconds"          --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_030

# experiment "63-hadooppm-day1-15seconds"          --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_015
#experiment "64-hadooppm-day1-60seconds"          --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_060
#experiment "65-hadooppm-day1-90seconds"          --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_090


#experiment "66-hadoop-day1-15seconds"            --nobrown --nogreen --peak 0.0 --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_015
#experiment "67-hadoop-day1-60seconds"            --nobrown --nogreen --peak 0.0 --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_060
#experiment "68-hadoop-day1-90seconds"            --nobrown --nogreen --peak 0.0 --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_090

# GreenOnly repeat
experiment "22-greenonly-day1-30seconds"          --energy --nobrown --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_SHOEB

# GreenVarPrices repeat
# experiment "30-greenvarprices-day1-30seconds"     --energy --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030

# Perfect vs unknown
# experiment "50-greenpeak-day1-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_030
# experiment "51-greenpeak-day2-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_2 --speedup 24 --brownfile $BROWN_2 --greenfile $SOLAR_2 -w $WORKLOAD_CONT_030
# experiment "53-greenpeak-day4-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_4 --speedup 24 --brownfile $BROWN_4 --greenfile $SOLAR_4 -w $WORKLOAD_CONT_030
# experiment "52-greenpeak-day3-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_3 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_3 -w $WORKLOAD_CONT_030

# Different days
# Abu Shoeb test cases
# experiment "55-greenpeak-day2-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_2 --speedup 24 --brownfile $BROWN_2 --greenfile $SOLAR_2 --pred -w $WORKLOAD_CONT_030
# experiment "56-greenpeak-day3-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_3 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_3 --pred -w $WORKLOAD_CONT_030
# experiment "57-greenpeak-day4-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_4 --speedup 24 --brownfile $BROWN_4 --greenfile $SOLAR_4 --pred -w $WORKLOAD_CONT_030

# experiment "54-greenpeak-day0-30seconds-perfect"    --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_0 --speedup 24 --brownfile $BROWN_0 --greenfile $SOLAR_0 -w $WORKLOAD_CONT_030
# experiment "58-greenpeak-day0-30seconds"            --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_0 --speedup 24 --brownfile $BROWN_0 --greenfile $SOLAR_0 --pred -w $WORKLOAD_CONT_030

# Asumption and utilization
# experiment "10-greenpeak-day1-15seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_015 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_015
# experiment "11-greenpeak-day1-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030
# experiment "12-greenpeak-day1-60seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_060 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_060
# experiment "13-greenpeak-day1-90seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_090 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_090

# experiment "60-greenpeak-day1-15seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_015 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_015
# experiment "61-greenpeak-day1-60seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_060 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_060
# experiment "62-greenpeak-day1-90seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_090 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_090



# Workloads
#experiment "77-1-hadooppm-day1-nutch2"                 --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_NUTCH -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_NUTCH2
#experiment "76-1-greenpeak-day1-nutch2"                --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_NUTCH -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_NUTCH2
#experiment "78-1-hadoop-day1-nutch2"                   --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_NUTCH2


# experiment "80-hadoop-day1-30seconds-high1"          --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH1
# experiment "81-hadooppm-day1-30seconds-high1"        --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH1
# experiment "82-greenpeak-day1-30seconds-high1"       --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH1

# experiment "90-hadoop-day1-30seconds"                 --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030
# experiment "91-hadoop-day2-30seconds"                 --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_2 --speedup 24 --brownfile $BROWN_2 --greenfile $SOLAR_2 --pred -w $WORKLOAD_CONT_030
# experiment "92-hadoop-day3-30seconds"                 --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_3 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_3 --pred -w $WORKLOAD_CONT_030
# experiment "93-hadoop-day4-30seconds"                 --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_4 --speedup 24 --brownfile $BROWN_4 --greenfile $SOLAR_4 --pred -w $WORKLOAD_CONT_030
# experiment "94-hadoop-day0-30seconds"                 --nobrown --nogreen --peak 0.0 --repl 0 --taskenergy $TASK_ENERGY_NUTCH -d $DATE_0 --speedup 24 --brownfile $BROWN_0 --greenfile $SOLAR_0 --pred -w $WORKLOAD_CONT_030

# experiment "83-hadoop-day1-30seconds-high2"          --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH2
# experiment "84-hadooppm-day1-30seconds-high2"        --energy --nobrown --nogreen --peak 0.0  --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH2
# experiment "85-greenpeak-day1-30seconds-high2"       --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH2

# repeat
# experiment "20-r-hadoop-day1-30seconds"             --nobrown --nogreen --peak 0.0 --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_030
# experiment "21-hadooppm-day1-30seconds"             --energy --nobrown --nogreen --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 -w $WORKLOAD_CONT_030
# experiment "22-r-greenonly-day1-30seconds"          --energy --nobrown --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030
# experiment "30-r-greenvarprices-day1-30seconds"     --energy --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030
# experiment "11-r-greenpeak-day1-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030

# experiment "30-testdeadline900-greenvarprices-day1-30seconds"     --energy --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030

# experiment "82-greenpeak-day1-30seconds-high1"       --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH1
# experiment "85-greenpeak-day1-30seconds-high2"       --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_HIGH2

# Abu Shoeb modified
# cp ghadoop.py.deadline900 ghadoop.py
# experiment "11-900secs-greenpeak-day1-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030 --deadline 900


# cp ghadoop.py.deadline1800 ghadoop.py
# experiment "11-1800secs-greenpeak-day1-30seconds"          --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_030

# cp ghadoop.py.regular ghadoop.py
